package com.investimentos.aplicacao;

import org.springframework.data.repository.CrudRepository;

public interface TransacaoRepository extends CrudRepository<Transacao, Integer> {

}
